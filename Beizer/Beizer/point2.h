#pragma once
#include "glut.h"

class Point2
{
public:
	Point2(){ yPoint = 0.0; xPoint = 0.0;}
	double getXPoint(){ return xPoint;}
	double getYPoint(){ return yPoint;}
	void setXPoint(double point) { xPoint = point;}
	void setYPoint(double point) { yPoint = point;}
private:
	double xPoint, yPoint;
};