#include "glut.h"
#include "bezier.h"
#include "proto.h"
Bezier::Bezier()
{
	Point2 points[4];
	Point2 cords[20];
}
Point2 Bezier::Evaluate(double t) // given parameter t between 0 and 1, finds the interpolated point.
{
	double px, py;
	Point2 p;
	//p= p0*(1-t)*(1-t)*(1-t) + 3*p1*(1-t)*(1-t)*t + 3*p2*(1-t)*t*t + p3*t*t*t;
	px = points[0].getXPoint()*(1-t)*(1-t)*(1-t) + 3*points[1].getXPoint()*(1-t)*(1-t)*t + 3*points[2].getXPoint()*(1-t)*t*t + points[3].getXPoint()*t*t*t;
	py = points[0].getYPoint()*(1-t)*(1-t)*(1-t) + 3*points[1].getYPoint()*(1-t)*(1-t)*t + 3*points[2].getYPoint()*(1-t)*t*t + points[3].getYPoint()*t*t*t;
	p.setXPoint(px);
	p.setYPoint(py);
	return p;
}

void Bezier::DrawControlPoints() // draws the 4 control points as circles.
{
	for(int i = 0; i < 4; i++)//draw each of the 4 circles here.
	{
		//glColor3d(0,0,1);
		DrawCircle(points[i].getXPoint(), points[i].getYPoint(), radius);
	}
}

void Bezier::DrawCurve() // draws the curve by approximating it at about 20 evaluation points.
{
	Point2 p;
	for(int i = 0; i < 20; i++)
	{
		p = Evaluate((double)i/(19));
		cords[i] = p;

	}
	
	DrawLine(cords);
}

int Bezier::IsPicked(double x,double y)//not working apparently...
{
	int radius1 = radius*2;
	int n = -1;

	for(int i =0; i < 4; i++)
	{
		if(((x >= (points[i].getXPoint() - radius1)) && (x <= (points[i].getXPoint() + radius1))) && ((y >= (points[i].getYPoint() - radius1)) && (y <= (points[i].getYPoint() + radius1))))
		{
			n = i;
		}
	}
	return n;
}