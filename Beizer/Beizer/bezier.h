#pragma once
#include "point2.h"
class Bezier
{
public:
	Bezier();
	Point2 Evaluate(double t);
	void DrawCurve();
	void DrawControlPoints();
	int IsPicked(double x,double y);
	Point2 points[4];
	Point2 cords[20];
	static const int radius = 10;
private:
	
};