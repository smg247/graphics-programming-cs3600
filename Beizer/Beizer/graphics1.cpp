// OpenGL/GLUT starter kit for Windows 7 and Visual Studio 2010
// Created spring, 2011
//
// This is a starting point for OpenGl applications.
// Add code to the "display" function below, or otherwise
// modify this file to get your desired results.
//
// For the first assignment, add this file to an empty Windows Console project
//		and then compile and run it as is.
// NOTE: You should also have glut.h,
// glut32.dll, and glut32.lib in the directory of your project.
// OR, see GlutDirectories.txt for a better place to put them.

#include <cmath>
#include <cstring>
#include <iostream>
#include "glut.h"
#include "bezier.h"
#include "point2.h"
// Global Variables (Only what you need!)
double screen_x = 700;
double screen_y = 500;
Bezier first = Bezier();
Bezier second = Bezier();
Bezier third = Bezier();
Bezier fourth = Bezier();
Bezier fifth = Bezier();
bool secondTest = false;
bool thirdTest = false;
bool fourthTest = false;
bool fifthTest = false;
int gPickedPoint1;
int gPickedPoint2;
int gPickedPoint3;
int gPickedPoint4;
int gPickedPoint5;
int gMovedPoint1;
int gMovedPoint2;
int gMovedPoint3;
int gMovedPoint4;
int gMovedPoint5;
double firstColor[3] = {1,0,0};
double secondColor[3] = {0,1,0};
double thirdColor[3] = {0,0,1};
double fourthColor[3] = {1,1,0};
double fifthColor[3] = {1,0,1};
bool color[5];
bool DrawCP = true;
double xStart1[5];
double yStart1[5];
double xStart2[5];
double yStart2[5];
double xStart3[5];
double yStart3[5];
double xStart4[5];
double yStart4[5];
double xStart5[5];
double yStart5[5];
double xOrg;
double yOrg;

enum MENU_TYPE {MENU_RED,MENU_GREEN,MENU_BLUE,MENU_YELLOW,MENU_PURPLE};
MENU_TYPE show = MENU_RED;

// 
// Functions that draw basic primitives
//
void DrawCircle(double x1, double y1, double radius)
{
	glBegin(GL_POLYGON);
	for(int i=0; i<32; i++)
	{
		double theta = (double)i/32.0 * 2.0 * 3.1415926;
		double x = x1 + radius * cos(theta);
		double y = y1 + radius * sin(theta);
		glVertex2d(x, y);
	}
	glEnd();
}

void DrawRectangle(double x1, double y1, double x2, double y2)
{
	glBegin(GL_QUADS);
	glVertex2d(x1,y1);
	glVertex2d(x2,y1);
	glVertex2d(x2,y2);
	glVertex2d(x1,y2);
	glEnd();
}

void DrawTriangle(double x1, double y1, double x2, double y2, double x3, double y3)
{
	glBegin(GL_TRIANGLES);
	glVertex2d(x1,y1);
	glVertex2d(x2,y2);
	glVertex2d(x3,y3);
	glEnd();
}

void DrawLine(Point2 cord[20])
{
	glBegin(GL_LINE_STRIP);
	for(int i = 0; i < 20; i++)
	{
		glVertex2d(cord[i].getXPoint(), cord[i].getYPoint());
	}
	glEnd();
}
// Outputs a string of text at the specified location.
void DrawText(double x, double y, char *string)
{
	void *font = GLUT_BITMAP_9_BY_15;

	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glEnable(GL_BLEND);

	int len, i;
	glRasterPos2d(x, y);
	len = (int) strlen(string);
	for (i = 0; i < len; i++) 
	{
		glutBitmapCharacter(font, string[i]);
	}

	glDisable(GL_BLEND);
}


//
// GLUT callback functions
//

// This callback function gets called by the Glut
// system whenever it decides things need to be redrawn.
void display(void)
{
	glClear(GL_COLOR_BUFFER_BIT);

	glColor3d(firstColor[0],firstColor[1],firstColor[2]);
	if(DrawCP)
	{
	first.DrawControlPoints();
	}
	first.DrawCurve();
	glColor3d(0,0,0);
		for(int i = 1; i < 5; i++)
		{
			if(DrawCP)
			DrawText(first.points[i-1].getXPoint(),first.points[i-1].getYPoint(), "1");
		}
	if(secondTest)
	{
		
		glColor3d(secondColor[0],secondColor[1],secondColor[2]);
		if(DrawCP)
		second.DrawControlPoints();
		second.DrawCurve();
		glColor3d(0,0,0);
		for(int i = 1; i < 5; i++)
		{
			if(DrawCP)
			DrawText(second.points[i-1].getXPoint(),second.points[i-1].getYPoint(), "2");
		}
	}
	if(thirdTest)
	{
		glColor3d(thirdColor[0],thirdColor[1],thirdColor[2]);
		if(DrawCP)
		third.DrawControlPoints();

		third.DrawCurve();

		glColor3d(0,0,0);
		for(int i = 1; i < 5; i++)
		{
			if(DrawCP)
			DrawText(third.points[i-1].getXPoint(),third.points[i-1].getYPoint(), "3");
		}
	}
	if(fourthTest)
	{
		glColor3d(fourthColor[0],fourthColor[1],fourthColor[2]);
		if(DrawCP)
		fourth.DrawControlPoints();

		fourth.DrawCurve();

		glColor3d(0,0,0);
		for(int i = 1; i < 5; i++)
		{
			if(DrawCP)
			DrawText(fourth.points[i-1].getXPoint(),fourth.points[i-1].getYPoint(), "4");
		}
	}
	if(fifthTest)
	{
		glColor3d(fifthColor[0],fifthColor[1],fifthColor[2]);
		if(DrawCP)
		fifth.DrawControlPoints();

		fifth.DrawCurve();

		glColor3d(0,0,0);
		for(int i = 1; i < 5; i++)
		{
			if(DrawCP)
			DrawText(fifth.points[i-1].getXPoint(),fifth.points[i-1].getYPoint(), "5");
		}
	}
	glutSwapBuffers();
	glutPostRedisplay();
}


// This callback function gets called by the Glut
// system whenever a key is pressed.
void keyboard(unsigned char c, int x, int y)
{
	switch (c) 
	{
	case 27: // escape character means to quit the program
		exit(0);
		break;
	case '1':
		color[1] = false;
		color[0] = true;
		color[2] = false;
		color[3] = false;
		color[4] = false;
		break;
	case '2':
		if(!secondTest)
		{
			secondTest = true;
		}
		color[1] = true;
		color[0] = false;
		color[2] = false;
		color[3] = false;
		color[4] = false;
		break;
	case '3':
		if(!thirdTest)
		{
			thirdTest = true;
		}
		color[1] = false;
		color[0] = false;
		color[2] = true;
		color[3] = false;
		color[4] = false;
		break;
	case '4':
		if(!fourthTest)
		{
			fourthTest = true;
		}
		color[1] = false;
		color[0] = false;
		color[2] = false;
		color[3] = true;
		color[4] = false;
		break;
	case '5':
		if(!fifthTest)
		{
			fifthTest = true;
		}
		color[1] = false;
		color[0] = false;
		color[2] = false;
		color[3] = false;
		color[4] = true;
		break;
	case 'r':
		if(color[0] == true)
		{
			firstColor[0] = 1;
			firstColor[1] = 0;
			firstColor[2] = 0;
		}
		if(color[1] == true)
		{
			secondColor[0] = 1;
			secondColor[1] = 0;
			secondColor[2] = 0;
		}
		if(color[2] == true)
		{
			thirdColor[0] = 1;
			thirdColor[1] = 0;
			thirdColor[2] = 0;
		}
		if(color[3] == true)
		{
			fourthColor[0] = 1;
			fourthColor[1] = 0;
			fourthColor[2] = 0;
		}
		if(color[4] == true)
		{
			fifthColor[0] = 1;
			fifthColor[1] = 0;
			fifthColor[2] = 0;
		}

		break;
	case 'g':
		if(color[0] == true)
		{
			firstColor[0] = 0;
			firstColor[1] = 1;
			firstColor[2] = 0;
		}
		if(color[1] == true)
		{
			secondColor[0] = 0;
			secondColor[1] = 1;
			secondColor[2] = 0;
		}
		if(color[2] == true)
		{
			thirdColor[0] = 0;
			thirdColor[1] = 1;
			thirdColor[2] = 0;
		}
		if(color[3] == true)
		{
			fourthColor[0] = 0;
			fourthColor[1] = 1;
			fourthColor[2] = 0;
		}
		if(color[4] == true)
		{
			fifthColor[0] = 0;
			fifthColor[1] = 1;
			fifthColor[2] = 0;
		}
		break;
	case 'b':
		if(color[0] == true)
		{
			firstColor[0] = 0;
			firstColor[1] = 0;
			firstColor[2] = 1;
		}
		if(color[1] == true)
		{
			secondColor[0] = 0;
			secondColor[1] = 0;
			secondColor[2] = 1;
		}
		if(color[2] == true)
		{
			thirdColor[0] = 0;
			thirdColor[1] = 0;
			thirdColor[2] = 1;
		}
		if(color[3] == true)
		{
			fourthColor[0] = 0;
			fourthColor[1] = 0;
			fourthColor[2] = 1;
		}
		if(color[4] == true)
		{
			fifthColor[0] = 0;
			fifthColor[1] = 0;
			fifthColor[2] = 1;
		}
		break;
	case 'y':
		if(color[0] == true)
		{
			firstColor[0] = 1;
			firstColor[1] = 1;
			firstColor[2] = 0;
		}
		if(color[1] == true)
		{
			secondColor[0] = 1;
			secondColor[1] = 1;
			secondColor[2] = 0;
		}
		if(color[2] == true)
		{
			thirdColor[0] = 1;
			thirdColor[1] = 1;
			thirdColor[2] = 0;
		}
		if(color[3] == true)
		{
			fourthColor[0] = 1;
			fourthColor[1] = 1;
			fourthColor[2] = 0;
		}
		if(color[4] == true)
		{
			fifthColor[0] = 1;
			fifthColor[1] = 1;
			fifthColor[2] = 0;
		}
		break;
	case 'p':
		if(color[0] == true)
		{
			firstColor[0] = 1;
			firstColor[1] = 0;
			firstColor[2] = 1;
		}
		if(color[1] == true)
		{
			secondColor[0] = 1;
			secondColor[1] = 0;
			secondColor[2] = 1;
		}
		if(color[2] == true)
		{
			thirdColor[0] = 1;
			thirdColor[1] = 0;
			thirdColor[2] = 1;
		}
		if(color[3] == true)
		{
			fourthColor[0] = 1;
			fourthColor[1] = 0;
			fourthColor[2] = 1;
		}
		if(color[4] == true)
		{
			fifthColor[0] = 1;
			fifthColor[1] = 0;
			fifthColor[2] = 1;
		}
		break;
	case 'd':
		if(DrawCP)
		{
			DrawCP = false;
		}
		else
		{
			DrawCP = true;
		}
		break;
	default:
		return; // if we don't care, return without glutPostRedisplay()
	}

	glutPostRedisplay();
}


// This callback function gets called by the Glut
// system whenever the window is resized by the user.
void reshape(int w, int h)
{
	// Reset our global variables to the new width and height.
	screen_x = w;
	screen_y = h;

	// Set the pixel resolution of the final picture (Screen coordinates).
	glViewport(0, 0, w, h);

	// Set the projection mode to 2D orthographic, and set the world coordinates:
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluOrtho2D(0, w, 0, h);
	glMatrixMode(GL_MODELVIEW);

}

// This callback function gets called by the Glut
// system whenever any mouse button goes up or down.
void mouse(int mouse_button, int state, int x, int y)
{
	y = screen_y - y;
	if (mouse_button == GLUT_LEFT_BUTTON && state == GLUT_DOWN) 
	{
		gPickedPoint1 = (first.IsPicked((double)x,(double)y));
		gPickedPoint2 = (second.IsPicked((double)x,(double)y));
		gPickedPoint3 = (third.IsPicked((double)x,(double)y));
		gPickedPoint4 = (fourth.IsPicked((double)x,(double)y));
		gPickedPoint5 = (fifth.IsPicked((double)x,(double)y));

	}
	if (mouse_button == GLUT_LEFT_BUTTON && state == GLUT_UP) 
	{
		gPickedPoint1 = -1;
		gPickedPoint2 = -1;
		gPickedPoint3 = -1;
		gPickedPoint4 = -1;
		gPickedPoint5 = -1;
	}
	if (mouse_button == GLUT_MIDDLE_BUTTON && state == GLUT_DOWN) 
	{
		xOrg = x;
		yOrg = y;
		
		gMovedPoint1 = (first.IsPicked((double)x,(double)y));
		gMovedPoint2 = (second.IsPicked((double)x,(double)y));
		gMovedPoint3 = (third.IsPicked((double)x,(double)y));
		gMovedPoint4 = (fourth.IsPicked((double)x,(double)y));
		gMovedPoint5 = (fifth.IsPicked((double)x,(double)y));

		if(gMovedPoint1 != -1)
			{
				for(int i = 0; i < 4; i++)
				{
					xStart1[i] = first.points[i].getXPoint();
					yStart1[i] = first.points[i].getYPoint();
				}
			}
		if(gMovedPoint2 != -1)
			{
		for(int i = 0; i < 4; i++)
		{
			xStart2[i] = second.points[i].getXPoint();
			yStart2[i] = second.points[i].getYPoint();
		}
		}
		if(gMovedPoint3 != -1)
			{
		for(int i = 0; i < 4; i++)
		{
			xStart3[i] = third.points[i].getXPoint();
			yStart3[i] = third.points[i].getYPoint();
		}
		}
		if(gMovedPoint4 != -1)
			{
		for(int i = 0; i < 4; i++)
		{
			xStart4[i] = fourth.points[i].getXPoint();
			yStart4[i] = fourth.points[i].getYPoint();
		}
		}
		if(gMovedPoint5 != -1)
			{
		for(int i = 0; i < 4; i++)
		{
			xStart5[i] = fifth.points[i].getXPoint();
			yStart5[i] = fifth.points[i].getYPoint();
		}
		}

	}
	if (mouse_button == GLUT_MIDDLE_BUTTON && state == GLUT_UP) 
	{
		gMovedPoint1 = -1;
		gMovedPoint2 = -1;
		gMovedPoint3 = -1;
		gMovedPoint4 = -1;
		gMovedPoint5 = -1;
	}
	if (mouse_button == GLUT_RIGHT_BUTTON && state == GLUT_DOWN) 
	{
	}
	if (mouse_button == GLUT_RIGHT_BUTTON && state == GLUT_UP) 
	{

	}
	glutPostRedisplay();
}

void motion(int x, int y)
{
	y = screen_y - y;
	if(gPickedPoint1 != -1)
	{
		first.points[gPickedPoint1].setXPoint((double)x);
		first.points[gPickedPoint1].setYPoint((double)y);
	}
	if(gPickedPoint2 != -1)
	{
		second.points[gPickedPoint2].setXPoint((double)x);
		second.points[gPickedPoint2].setYPoint((double)y);
	}
	if(gPickedPoint3 != -1)
	{
		third.points[gPickedPoint3].setXPoint((double)x);
		third.points[gPickedPoint3].setYPoint((double)y);
	}
	if(gPickedPoint4 != -1)
	{
		fourth.points[gPickedPoint4].setXPoint((double)x);
		fourth.points[gPickedPoint4].setYPoint((double)y);
	}
	if(gPickedPoint5 != -1)
	{
		fifth.points[gPickedPoint5].setXPoint((double)x);
		fifth.points[gPickedPoint5].setYPoint((double)y);
	}

	if(gMovedPoint1 != -1)
	{
		for(int i = 0; i < 4; i++)
		{
			first.points[i].setXPoint(xStart1[i] + x - xOrg);
			first.points[i].setYPoint(yStart1[i] + y - yOrg);
		}
	}
	if(gMovedPoint2 != -1)
	{
		for(int i = 0; i < 4; i++)
		{
			second.points[i].setXPoint(xStart2[i] + x - xOrg);
			second.points[i].setYPoint(yStart2[i] + y - yOrg);
		}
	}
	if(gMovedPoint3 != -1)
	{
		for(int i = 0; i < 4; i++)
		{
			third.points[i].setXPoint(xStart3[i] + x - xOrg);
			third.points[i].setYPoint(yStart3[i] + y - yOrg);
		}
	}
	if(gMovedPoint4 != -1)
	{
		for(int i = 0; i < 4; i++)
		{
			fourth.points[i].setXPoint(xStart4[i] + x - xOrg);
			fourth.points[i].setYPoint(yStart4[i] + y - yOrg);
		}
	}
	if(gMovedPoint5 != -1)
	{
		for(int i = 0; i < 4; i++)
		{
			fifth.points[i].setXPoint(xStart5[i] + x - xOrg);
			fifth.points[i].setYPoint(yStart5[i] + y - yOrg);
		}
	}
}
// Your initialization code goes here.
void InitializeMyStuff()
{
	int a1x = 100;
	int a1y = 300;
	int a2x = 200;
	int a2y = 100;
	int a3x = 300;
	int a3y = 300;
	int a4x = 400;
	int a4y = 200;
	first.points[0].setXPoint(a1x);
	first.points[0].setYPoint(a1y);
	first.points[1].setXPoint(a2x);
	first.points[1].setYPoint(a2y);
	first.points[2].setXPoint(a3x);
	first.points[2].setYPoint(a3y);
	first.points[3].setXPoint(a4x);
	first.points[3].setYPoint(a4y);

	second.points[0].setXPoint(50);
	second.points[0].setYPoint(100);
	second.points[1].setXPoint(100);
	second.points[1].setYPoint(150);
	second.points[2].setXPoint(150);
	second.points[2].setYPoint(50);
	second.points[3].setXPoint(250);
	second.points[3].setYPoint(100);

	third.points[0].setXPoint(150);
	third.points[0].setYPoint(200);
	third.points[1].setXPoint(75);
	third.points[1].setYPoint(300);
	third.points[2].setXPoint(250);
	third.points[2].setYPoint(400);
	third.points[3].setXPoint(240);
	third.points[3].setYPoint(200);

	fourth.points[0].setXPoint(500);
	fourth.points[0].setYPoint(100);
	fourth.points[1].setXPoint(140);
	fourth.points[1].setYPoint(130);
	fourth.points[2].setXPoint(170);
	fourth.points[2].setYPoint(150);
	fourth.points[3].setXPoint(230);
	fourth.points[3].setYPoint(90);

	fifth.points[0].setXPoint(120);
	fifth.points[0].setYPoint(200);
	fifth.points[1].setXPoint(160);
	fifth.points[1].setYPoint(300);
	fifth.points[2].setXPoint(400);
	fifth.points[2].setYPoint(400);
	fifth.points[3].setXPoint(230);
	fifth.points[3].setYPoint(300);

	// Add menu items
	glutAddMenuEntry("Red", MENU_RED);
	glutAddMenuEntry("Green", MENU_GREEN);
	glutAddMenuEntry("Blue", MENU_BLUE);
	glutAddMenuEntry("Yellow", MENU_YELLOW);
	glutAddMenuEntry("Purple", MENU_PURPLE);

	// Associate a mouse button with menu
	glutAttachMenu(GLUT_RIGHT_BUTTON);
}

void menu(int item)
{
	switch (item)
	{
	case MENU_RED:
		{
			if(color[0] == true)
		{
			firstColor[0] = 1;
			firstColor[1] = 0;
			firstColor[2] = 0;
		}
		if(color[1] == true)
		{
			secondColor[0] = 1;
			secondColor[1] = 0;
			secondColor[2] = 0;
		}
		if(color[2] == true)
		{
			thirdColor[0] = 1;
			thirdColor[1] = 0;
			thirdColor[2] = 0;
		}
		if(color[3] == true)
		{
			fourthColor[0] = 1;
			fourthColor[1] = 0;
			fourthColor[2] = 0;
		}
		if(color[4] == true)
		{
			fifthColor[0] = 1;
			fifthColor[1] = 0;
			fifthColor[2] = 0;
		}
			show = (MENU_TYPE) item;
		}
		break;
	case MENU_GREEN:
		{
			if(color[0] == true)
		{
			firstColor[0] = 0;
			firstColor[1] = 1;
			firstColor[2] = 0;
		}
		if(color[1] == true)
		{
			secondColor[0] = 0;
			secondColor[1] = 1;
			secondColor[2] = 0;
		}
		if(color[2] == true)
		{
			thirdColor[0] = 0;
			thirdColor[1] = 1;
			thirdColor[2] = 0;
		}
		if(color[3] == true)
		{
			fourthColor[0] = 0;
			fourthColor[1] = 1;
			fourthColor[2] = 0;
		}
		if(color[4] == true)
		{
			fifthColor[0] = 0;
			fifthColor[1] = 1;
			fifthColor[2] = 0;
		}
			show = (MENU_TYPE) item;
		}
		break;
	case MENU_BLUE:
		{
			if(color[0] == true)
		{
			firstColor[0] = 0;
			firstColor[1] = 0;
			firstColor[2] = 1;
		}
		if(color[1] == true)
		{
			secondColor[0] = 0;
			secondColor[1] = 0;
			secondColor[2] = 1;
		}
		if(color[2] == true)
		{
			thirdColor[0] = 0;
			thirdColor[1] = 0;
			thirdColor[2] = 1;
		}
		if(color[3] == true)
		{
			fourthColor[0] = 0;
			fourthColor[1] = 0;
			fourthColor[2] = 1;
		}
		if(color[4] == true)
		{
			fifthColor[0] = 0;
			fifthColor[1] = 0;
			fifthColor[2] = 1;
		}
			show = (MENU_TYPE) item;
		}
		break;
	case MENU_YELLOW:
		{
			if(color[0] == true)
		{
			firstColor[0] = 1;
			firstColor[1] = 1;
			firstColor[2] = 0;
		}
		if(color[1] == true)
		{
			secondColor[0] = 1;
			secondColor[1] = 1;
			secondColor[2] = 0;
		}
		if(color[2] == true)
		{
			thirdColor[0] = 1;
			thirdColor[1] = 1;
			thirdColor[2] = 0;
		}
		if(color[3] == true)
		{
			fourthColor[0] = 1;
			fourthColor[1] = 1;
			fourthColor[2] = 0;
		}
		if(color[4] == true)
		{
			fifthColor[0] = 1;
			fifthColor[1] = 1;
			fifthColor[2] = 0;
		}
			show = (MENU_TYPE) item;
		}
		break;
	case MENU_PURPLE:
		{
			if(color[0] == true)
		{
			firstColor[0] = 1;
			firstColor[1] = 0;
			firstColor[2] = 1;
		}
		if(color[1] == true)
		{
			secondColor[0] = 1;
			secondColor[1] = 0;
			secondColor[2] = 1;
		}
		if(color[2] == true)
		{
			thirdColor[0] = 1;
			thirdColor[1] = 0;
			thirdColor[2] = 1;
		}
		if(color[3] == true)
		{
			fourthColor[0] = 1;
			fourthColor[1] = 0;
			fourthColor[2] = 1;
		}
		if(color[4] == true)
		{
			fifthColor[0] = 1;
			fifthColor[1] = 0;
			fifthColor[2] = 1;
		}
			show = (MENU_TYPE) item;
		}
		break;
	default:
		{ 
		}
		break;
	}

	glutPostRedisplay();

	return;
}

int main(int argc, char **argv)
{
	std::cout << "Use the '1-5' keys to create new curves---use the 'r,g,b,y, and p' keys to" << std::endl << "change the color of "
		"the last curve whoose key you pressed, or right click to" << std::endl << "change the color with a menu. Also, press 'd' to toggle control points." << std::endl;
	system("PAUSE");
	glutInit(&argc, argv);

	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);
	glutInitWindowSize(screen_x, screen_y);
	glutInitWindowPosition(50, 50);

	int fullscreen = 0;
	if (fullscreen) 
	{
		glutGameModeString("800x600:32");
		glutEnterGameMode();
	} 
	else 
	{
		glutCreateWindow("Bezier Curves");
	}

	glutDisplayFunc(display);
	glutKeyboardFunc(keyboard);
	glutReshapeFunc(reshape);
	glutMotionFunc(motion);
	glutMouseFunc(mouse);
	glutCreateMenu(menu);

	glColor3d(0,0,0); // forground color
	glClearColor(.75, .75, .75, 0); // background color
	InitializeMyStuff();

	glutMainLoop();

	return 0;
}
