
#include <cmath>
using namespace std;
#include "graphics.h"
#include "circle.h"
#include "glut.h"
#include "collision.h"
Circle::Circle()
{
	mX = 0;
	mY = 0;
	mRadious = 0;
	mR = 0;
	mG = 0;
	mB = 0;
	dx = 0;
	dy = 0;
	mNUM_CIRCLES = 10;
	zombies = false;
}
Circle::Circle(int x1, int y1, int x2, int y2, int rad1, int rad2, double r, double g, double b, int dir)
{
	mX = x1+x2;
	mY = y1+y2;
	mRadious = rad1 + rad2;
	mR = r;
	mG = g;
	mB = b;
	mNUM_CIRCLES = 10;
	if(dir == 1)//this makes the circles start off going a random direction
	{
		dy = .05;
		dx = .07;
	}
	else if( dir == 2)
	{
		dy = -.05;
		dx = -.07;
	}
	else if(dir == 3)
	{
		dy = -.05;
		dx = .07;
	}
	else
	{
		dy = .05;
		dx = -.07;
	}
}
void Circle::Draw()
{
	glColor3d(mR, mG, mB);
	DrawCircle(mX, mY, mRadious);

}
void Circle::setdx(double x)
{
	dx = x;
}
void Circle::setdy(double y)
{
	dy = y;
}
double Circle::getSize()
{
	return mRadious*2;
}
double Circle::getnextx()
{
	return mX + dx;
}
double Circle::getnexty()
{
	return mY + dy;
}
double Circle::getdx()
{
	return dx;
}
double Circle::getdy()
{
	return dy;
}
double Circle::getradius()
{
	return mRadious;
}
double Circle::getX()
{
	return mX;
}
double Circle::getY()
{
	return mY;
}
void Circle::Update(double screen_x, double screen_y, int me, int size, Circle particles[])
{
	//Add Friciton
	const double AIR_FRICTION = .999;//turn back to .9999!
	dx *= AIR_FRICTION;
	dy *= AIR_FRICTION;
	//Add Gravity
	const double GRAVITY_CONSTANT = .01;
	dy -= GRAVITY_CONSTANT;
	//Ball-Ball collisions
	for(int i = 0; i < mNUM_CIRCLES; i++)
	{
		for(int j = i+1; j < mNUM_CIRCLES; j++)
		{
			double xdif=particles[i].getnextx() - particles[j].getnextx();
			double ydif=particles[i].getnexty() - particles[j].getnexty();
			double size=sqrt(xdif*xdif + ydif*ydif);
			double radSum = particles[i].getradius() + particles[j].getradius();
			if(size < radSum)
			{
				//if(particles[j].mR == particles[i].mR || particles [j].mB == particles[i].mB)// makes them only bounce off the same color
				//{
				Collide(i,j, particles);
				//}
				//the following makes the colors of the circles swap when they collide
				double tempR = particles[i].mR;
				double tempB = particles[i].mB;
				double tempG = particles[i].mG;
				particles[i].mR = particles[j].mR;
				particles[i].mB = particles[j].mB;
				particles[i].mG = particles[j].mG;
				if(!zombies)
				{
					particles[j].mR = tempR;
					particles[j].mB = tempB;
					particles[j].mG = tempG;
				}
			}
		}
	}
	//Ball-Wall collision detection and handling
	const double WALL_FRICTION = .01;
	if(mY+dy+mRadious > screen_y)//where they are going to be, not where they are
	{
		dy = -fabs(dy + WALL_FRICTION);//force it to turn around
	}
	if(mX+dx+mRadious > screen_x)
	{
		dx = -fabs(dx - WALL_FRICTION);
	}
	if(mX+dx-mRadious< 0)
	{
		dx = fabs(dx - WALL_FRICTION);
	}
	if(mY+dy-mRadious < 0)
	{
		dy = fabs(dy - WALL_FRICTION);
	}
	//Incremiment circles position according to its velocity
	mX += dx;
	mY += dy;
	Draw();
}
