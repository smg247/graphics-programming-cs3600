class CodeCollisionDetection
{
public:
	double getx() {return x;}
	double gety() {return y;}
	double getradius() {return radius;}
	double getr() {return r;};
	double getg() {return g;};
	double getb() {return b;};
	double getdx() {return xDir;}
	double getdy() {return yDir;}
	double getnextx() { return x + xDir; }
	double getnexty() { return y + yDir; }

	void setdx(double dx) { xDir = dx; }
	void setdy(double dy) { yDir = dy; }

	void step(int myself, my_circle all[], int count);
private:
	double x,y,radius;
	double xDir, yDir;
	double r,g,b;
};

struct vectortype
{
	double x;
	double y;
};

void Collide(int p1, int p2, my_circle particles[])