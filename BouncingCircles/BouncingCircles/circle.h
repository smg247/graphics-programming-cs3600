#pragma once
class Circle
{
public:
	Circle();
	Circle(int x1, int y1, int x2, int y2, int rad1, int rad2, double r, double g, double b, int dir);
	void Draw();
	void Update(double screen_x, double screen_y, int me, int size, Circle particles[]);
	void setdx(double x);
	void setdy(double y);
	double getSize();
	double getnextx();
	double getnexty();
	double getdx();
	double getdy();
	double getradius();
	double getX();
	double getY();
	int mNUM_CIRCLES;
	double mR,mG,mB;
	bool zombies;
private:
	double mX, mY, mRadious,dy,dx;
	
};

