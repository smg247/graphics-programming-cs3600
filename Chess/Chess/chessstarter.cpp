// (your name here)
// Chess animation starter kit.

#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <math.h>
#include <string.h>
#include <fstream>
#include <iostream>
#include <ctime>
using namespace std;
#include "glut.h"
#include "graphics.h"


// Global Variables
// Some colors you can use, or make your own and add them
// here and in graphics.h
GLdouble redMaterial[] = {0.7, 0.1, 0.2, 1.0};
GLdouble greenMaterial[] = {0.1, 0.7, 0.4, 1.0};
GLdouble brightGreenMaterial[] = {0.1, 0.9, 0.1, 1.0};
GLdouble blueMaterial[] = {0.1, 0.2, 0.7, 1.0};
GLdouble whiteMaterial[] = {1.0, 1.0, 1.0, 1.0};

double screen_x = 600;
double screen_y = 500;

static clock_t t;
const int M = 5;
const int N = 5;

// Outputs a string of text at the specified location.
void text_output(double x, double y, char *string)
{
	void *font = GLUT_BITMAP_9_BY_15;

	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glEnable(GL_BLEND);

	int len, i;
	glRasterPos2d(x, y);
	len = (int) strlen(string);
	for (i = 0; i < len; i++) 
	{
		glutBitmapCharacter(font, string[i]);
	}

	glDisable(GL_BLEND);
}

// Given the three triangle points x[0],y[0],z[0],
//		x[1],y[1],z[1], and x[2],y[2],z[2],
//		Finds the normal vector n[0], n[1], n[2].
void FindTriangleNormal(double x[], double y[], double z[], double n[])
{
	// Convert the 3 input points to 2 vectors, v1 and v2.
	double v1[3], v2[3];
	v1[0] = x[1] - x[0];
	v1[1] = y[1] - y[0];
	v1[2] = z[1] - z[0];
	v2[0] = x[2] - x[0];
	v2[1] = y[2] - y[0];
	v2[2] = z[2] - z[0];

	// Take the cross product of v1 and v2, to find the vector perpendicular to both.
	n[0] = v1[1]*v2[2] - v1[2]*v2[1];
	n[1] = -(v1[0]*v2[2] - v1[2]*v2[0]);
	n[2] = v1[0]*v2[1] - v1[1]*v2[0];

	double size = sqrt(n[0]*n[0] + n[1]*n[1] + n[2]*n[2]);
	n[0] /= -size;
	n[1] /= -size;
	n[2] /= -size;
}

// Loads the given data file and draws it at its default position.
// Call glTranslate before calling this to get it in the right place.
void DrawPiece(char filename[])
{
	// Try to open the given file.
	char buffer[200];
	ifstream in(filename);
	if(!in)
	{
		cerr << "Error. Could not open " << filename << endl;
		exit(1);
	}

	double x[100], y[100], z[100]; // stores a single polygon up to 100 vertices.
	int done = false;
	int verts = 0; // vertices in the current polygon
	int polygons = 0; // total polygons in this file.
	do
	{
		in.getline(buffer, 200); // get one line (point) from the file.
		int count = sscanf(buffer, "%lf, %lf, %lf", &(x[verts]), &(y[verts]), &(z[verts]));
		done = in.eof();
		if(!done)
		{
			if(count == 3) // if this line had an x,y,z point.
			{
				verts++;
			}
			else // the line was empty. Finish current polygon and start a new one.
			{
				if(verts>=3)
				{
					glBegin(GL_POLYGON);
					double n[3];
					FindTriangleNormal(x, y, z, n);
					glNormal3dv(n);
					for(int i=0; i<verts; i++)
					{
						glVertex3d(x[i], y[i], z[i]);
					}
					glEnd(); // end previous polygon
					polygons++;
					verts = 0;
				}
			}
		}
	}
	while(!done);

	if(verts>0)
	{
		cerr << "Error. Extra vertices in file " << filename << endl;
		exit(1);
	}

}
void DrawSquare(double i, double j)
{
	double z = 500;

	glBegin(GL_QUADS);
	glVertex3d(i,0,j);//left edge
	glVertex3d(i,0,j+1000);
	glVertex3d(i,z,j+1000);
	glVertex3d(i,z,j);
	glEnd();

	glBegin(GL_QUADS);//top edge
	glVertex3d(i,0,j+1000);
	glVertex3d(i+1000,0,j+1);
	glVertex3d(i+1000,z,j+1000);
	glVertex3d(i,z,j+1000);
	glEnd();

	glBegin(GL_QUADS);//right edge
	glVertex3d(i+1000,0,j+1000);
	glVertex3d(i+1000,0,j);
	glVertex3d(i+1000,z,j);
	glVertex3d(i+1000,z,j+1000);
	glEnd();

	glBegin(GL_QUADS);//bottom edge
	glVertex3d(i,0,j);
	glVertex3d(i+1000,0,j);
	glVertex3d(i+1000,z,j);
	glVertex3d(i,z,j);
	glEnd();

	glBegin(GL_QUADS);//top
	glVertex3d(i,z,j);
	glVertex3d(i,z,j+1000);
	glVertex3d(i+1000,z,j+1000);
	glVertex3d(i+1000,z,j);
	glEnd();
}
void DrawBoard(bool white)
{
	if(white)
	{
		for(int i = 1500; i < 8000; i +=2000)
		{
			for (int j = 500; j < 8000; j += 2000)
			{
				DrawSquare(i,j);
			}
		}

		for(int i = 500; i < 8000; i +=2000)
		{
			for (int j = 1500; j < 8000; j += 2000)
			{
				DrawSquare(i,j);
			}
		}
	}
	else
	{
		for(int i = 500; i < 8000; i +=2000)
		{
			for (int j = 500; j < 8000; j += 2000)
			{
				DrawSquare(i,j);
			}
		}

		for(int i = 1500; i < 8000; i +=2000)
		{
			for (int j = 1500; j < 8000; j += 2000)
			{
				DrawSquare(i,j);
			}
		}
	}
}


// NOTE: Y is the UP direction for the chess pieces.
double eye[3] = {4000, 3000, 1000}; // pick a nice vantage point.
double at[3]  = {4000, 500,     4000};
//
// GLUT callback functions
//
void Interpolate(double t1, double t, double t2, double x1, double &x, double x2)//moves the pieces
{
	if(t<t1)
	{
		x = x1;
		return;
	}
	if(t >t2)
	{
		x = x2;
		return;
	}
	double ratio = (t - t1)/(t2-t1);
	x = x1 + (x2-x1)*ratio;
}

// This callback function gets called by the Glut
// system whenever it decides things need to be redrawn.
void DrawLine(double x1, double y1, double x2, double y2)
{
	glBegin(GL_LINES);
	glVertex2d(x1, y1);
	glVertex2d(x2, y2);
	glEnd();
}
void display(void)
{

	//put the clock stuff here to find t
	static clock_t t1 = clock();
	clock_t t2 = clock();
	double t = ((double)t2-t1)/CLOCKS_PER_SEC;
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glLoadIdentity();
	gluLookAt(eye[0], eye[1], eye[2],  at[0], at[1], at[2],  0,1,0); // Y is up!

	// Set the color for one side (white), and draw its 16 pieces.
	GLfloat mat_amb_diff1[] = {0.8, 0.9, 0.5, 1.0};
	glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, mat_amb_diff1);
	DrawBoard(true);


	double yscale = 1.0;
	glPushMatrix();
	glTranslatef(3000, 500, 1000);
	Interpolate(10,t,12,1,yscale,0);
	glScaled(1, yscale, 1);
	if(t <=12)
	{
		glCallList(bishop);
	}
	glPopMatrix();

	glPushMatrix();
	glTranslatef(4000, 500, 1000);
	glCallList(king);
	glPopMatrix();

	glPushMatrix();
	double x = 0;
	double z = 0;
	if(t <=4)
	{
		
		Interpolate(2, t, 4, 1000, z, 6000);//t = clock
		glTranslatef(5000, 500, z);
	}
	else
	{
		
		Interpolate(4, t, 5, 5000, x, 7000);//t = clock
		glTranslatef(x, 500, 6000);
	}
	glCallList(queen);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(6000, 500, 1000);
	glCallList(bishop);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(7000, 500, 1000);
	glCallList(knight);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(2000, 500, 1000);
	glCallList(knight);
	glPopMatrix();

	glPushMatrix();
	double z3 = 0.0;
	Interpolate(19, t, 22, 1000, z3, 6000);
	glTranslatef(8000, 500, z3);
	glCallList(rook);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(1000, 500, 1000);
	glCallList(rook);
	glPopMatrix();

	for(int x=1000; x<=8000; x+=1000)
	{
		glPushMatrix();
		double z2 = 1.0;
		if(x == 3000)
		{
			Interpolate(12,t,14,2000,z2,4000);
			glTranslatef(x, 500, z2);

		}
		else if(x ==4000)
		{
			Interpolate(1,t,3,2000,z2,4000);
			glTranslatef(x, 500, z2);
			Interpolate(1,t,3,0,z2,-90);
			glRotated(z2,0,0,1);
		}
		else
		{
		glTranslatef(x, 500, 2000);
		}
		glCallList(pawn);
		glPopMatrix();
	}

	// Set the color for one side (black), and draw its 16 pieces.
	GLfloat mat_amb_diff2[] = {0.1, 0.5, 0.8, 1.0};
	glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, mat_amb_diff2);
	DrawBoard(false);
	glPushMatrix();//king
	glTranslatef(4000, 500, 8000);
	glCallList(king);
	glPopMatrix();

	glPushMatrix();//bishops
	glTranslatef(6000, 500, 8000);
	glCallList(bishop);
	glPopMatrix();

	glPushMatrix();
	double z1 = 0.0;
	Interpolate(7, t, 10, 8000, z1, 1000);
	glTranslatef(3000, 500, z1);
	glCallList(bishop);
	glPopMatrix();

	glPushMatrix();//queen
	glTranslatef(5000, 0, 8000);
	glCallList(queen);
	glPopMatrix();

	glPushMatrix();//knights
	glTranslatef(7000, 500, 8000);
	glRotated(180,0,1,0);
	glCallList(knight);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(2000, 500, 8000);
	glRotated(180,0,1,0);
	glCallList(knight);
	glPopMatrix();

	glPushMatrix();//rooks
	z1 = 0.0;
	Interpolate(16, t, 19, 8000, z1, 3000);
	glTranslatef(8000, 500, z1);
	glCallList(rook);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(1000, 500, 8000);
	glCallList(rook);
	glPopMatrix();

	for(int x=1000; x<=8000; x+=1000)//pawns
	{
		glPushMatrix();
		double z2 = 1.0;
		if(x == 1000)
		{
			Interpolate(14,t,16,7000,z2,5000);
			glTranslatef(x, 500, z2);
		}
		else
		{
		glTranslatef(x, 500, 7000);
		}
		glCallList(pawn);
		glPopMatrix();
	}


	glutSwapBuffers();
	glutPostRedisplay();
}


// This callback function gets called by the Glut
// system whenever a key is pressed.
void keyboard(unsigned char c, int x, int y)
{
	switch (c) 
	{
	case 27: // escape character means to quit the program
		exit(0);
		break;
	default:
		return; // if we don't care, return without glutPostRedisplay()
	}

	glutPostRedisplay();
}



void SetPerspectiveView(int w, int h)
{
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	double aspectRatio = (GLdouble) w/(GLdouble) h;
	gluPerspective( 
		/* field of view in degree */ 38.0,
		/* aspect ratio */ aspectRatio,
		/* Z near */ 100, /* Z far */ 30000.0);
	glMatrixMode(GL_MODELVIEW);
}

// This callback function gets called by the Glut
// system whenever the window is resized by the user.
void reshape(int w, int h)
{
	screen_x = w;
	screen_y = h;

	// Set the pixel resolution of the final picture (Screen coordinates).
	glViewport(0, 0, w, h);

	SetPerspectiveView(w,h);

}

// This callback function gets called by the Glut
// system whenever any mouse button goes up or down.
void mouse(int mouse_button, int state, int x, int y)
{
	if (mouse_button == GLUT_LEFT_BUTTON && state == GLUT_DOWN) 
	{
	}
	if (mouse_button == GLUT_LEFT_BUTTON && state == GLUT_UP) 
	{
	}
	if (mouse_button == GLUT_MIDDLE_BUTTON && state == GLUT_DOWN) 
	{
	}
	if (mouse_button == GLUT_MIDDLE_BUTTON && state == GLUT_UP) 
	{
	}
	glutPostRedisplay();
}

// Your initialization code goes here.
void InitializeMyStuff()
{
	// set material's specular properties
	GLfloat mat_specular[] = {1.0, 1.0, 1.0, 1.0};
	GLfloat mat_shininess[] = {50.0};
	glMaterialfv(GL_FRONT, GL_SPECULAR, mat_specular);
	glMaterialfv(GL_FRONT, GL_SHININESS, mat_shininess);

	// set light properties
	GLfloat light_position[] = {-eye[0], -eye[1], -eye[2],0};
	GLfloat white_light[] = {1,1,1,1};
	GLfloat low_light[] = {.3,.3,.3,1};
	glLightfv(GL_LIGHT0, GL_POSITION, light_position); // position first light
	glLightfv(GL_LIGHT0, GL_DIFFUSE, white_light); // specify first light's color
	glLightfv(GL_LIGHT0, GL_SPECULAR, low_light);

	glEnable(GL_DEPTH_TEST); // turn on depth buffering
	glEnable(GL_LIGHTING);	// enable general lighting
	glEnable(GL_LIGHT0);	// enable the first light.

	glNewList(pawn,GL_COMPILE);
	DrawPiece("PAWN.POL");
	glEndList();

	glNewList(king,GL_COMPILE);
	DrawPiece("KING.POL");
	glEndList();

	glNewList(queen,GL_COMPILE);
	DrawPiece("QUEEN.POL");
	glEndList();

	glNewList(rook,GL_COMPILE);
	DrawPiece("ROOK.POL");
	glEndList();

	glNewList(bishop,GL_COMPILE);
	DrawPiece("BISHOP.POL");
	glEndList();

	glNewList(knight,GL_COMPILE);
	DrawPiece("KNIGHT.POL");
	glEndList();

}


int main(int argc, char **argv)
{
	glutInit(&argc, argv);

	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
	glutInitWindowSize(screen_x, screen_y);
	glutInitWindowPosition(10, 10);

	int fullscreen = 0;
	if (fullscreen) 
	{
		glutGameModeString("800x600:32");
		glutEnterGameMode();
	} 
	else 
	{
		glutCreateWindow("Shapes");
	}

	glutDisplayFunc(display);
	glutKeyboardFunc(keyboard);
	glutReshapeFunc(reshape);
	glutMouseFunc(mouse);

	glClearColor(1,1,.5,1);	
	InitializeMyStuff();

	glutMainLoop();

	return 0;
}
