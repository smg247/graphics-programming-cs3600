#include <cmath>
using namespace std;
#include "glut.h"
#include "Rat.h"
#include "Maze.h"

Rat::Rat()
{
	rx = N/2;
	ry = M/2;
	rd = 0;
	dx = 0;
	dy = 0;
	legDegs = 45;
	radius = .2;
	gMaze = Maze();


}
void Rat::DrawRat()
{
	glPushMatrix();
	glTranslated(rx,ry, 0);
	glRotated(rd, 0,0,1);
	
	glColor3d(0.5,0.5,0.5);

	glBegin(GL_TRIANGLES);//draw rat body
	glVertex2d(.2,0);
	glVertex2d(-.2,.05);
	glVertex2d(-.2,-.05);
	glEnd();

		glColor3d(.5,.5,.5);
		glPushMatrix();
		glTranslated(0, -.025, 0);
		glRotated(legDegs,0,0,1);
		glTranslated(0,+.025,0);
		glBegin(GL_LINES);//draw left front rat leg
		glVertex2d(0,.025);
		glVertex2d(0,.2);
		glEnd();
		glPopMatrix();

		glPushMatrix();
		glTranslated(0, -.025, 0);
		glRotated(legDegs,0,0,-1);
		glTranslated(0,+.025,0);
		glBegin(GL_LINES);//draw right front rat leg
		glVertex2d(0,+.025);
		glVertex2d(0,-.2);
		glEnd();
		glPopMatrix();

	glPopMatrix();
}
void Rat::SpinLeft()
{
	rd += spinSize;
}
void Rat::SpinRight()
{
	rd -= spinSize;
}
void Rat::MoveForward()
{
	//if(gMaze.isSafe(getNextX(), getNextY(), getRadius()))
	//{
	dx = cos(rd*3.1416/180)*moveSize;
	dy = sin(rd*3.1416/180)*moveSize;
	rx += dx;
	ry += dy;

	/*}
	else//this is the code that gets called if it hits a wall
	{
		int nRY = (int)ry;//couldn't think of better var names :/
		int nRX = (int)rx;
		double goRx = rx - nRX;
		double goRy = ry - nRY;
		if(gMaze.isSafe(rx,getNextY(),getRadius()))//if up or down works, but right  or left does not
		{
			dy = sin(rd*3.1416/180)*moveSize;
			ry +=dy;
			if(goRx > .5)//right wall
				dx = -moveSize;
			if(goRx < .5)//left wall
				dx = moveSize;
			rx+=dx;
		}
		else if(gMaze.isSafe(getNextX(), ry, getRadius()))//if left or right work, but top or bottom dont
		{
			dx = cos(rd*3.1416/180)*moveSize;
			rx +=dx;
			if(goRy > .5)//top wall
				dy = -moveSize;
			if(goRy < .5)//bottom wall
				dy = moveSize;
			ry+=dy;
		}
		else
		{
			if(goRx > .5)
				rx = rx - .01;
			else
				rx = rx + .01;
			if(goRy > .5)
				ry = ry -.01;
			else
				ry = ry + .01;
		}
	}*/
}
double Rat::getNextX()
{
	dx = cos(rd*3.1416/180)*moveSize;
	dx = dx*20;
	return rx+dx;
}
double Rat::getNextY()
{
	dy = sin(rd*3.1416/180)*moveSize;
	dy = dy*20;
	return ry+dy;
}
double Rat::getRadius()
{
	return radius;
}