// OpenGL/GLUT starter kit for Windows 7 and Visual Studio 2010
// Created spring, 2011
//
// This is a starting point for OpenGl applications.
// Add code to the "display" function below, or otherwise
// modify this file to get your desired results.
//
// For the first assignment, add this file to an empty Windows Console project
//		and then compile and run it as is.
// NOTE: You should also have glut.h,
// glut32.dll, and glut32.lib in the directory of your project.
// OR, see GlutDirectories.txt for a better place to put them.

//This program creates a 2d maze
//Stephen Goeddel 
//February 2012
#include <cstdlib>//make exit not complain
#include <cmath>
#include <cstring>
#include <ctime>
#include <iostream>
using namespace std;
#include "glut.h"
#include "Cell.h"
#include "Maze.h"
#include "Graphics.h"
#include "Rat.h"


// Global Variables (Only what you need!)
double screen_x = 700;
double screen_y = 700;
//Maze gMaze = Maze();
Rat gRat = Rat();
bool gLeftButtonDown = false;
bool gMiddleButtonDown = false;
bool gRightButtonDown = false;
double legsDelta = 1.0;
double eyeLvl = .1;
bool top = false;
bool stad = true;
bool first = false;
double deltaT;
double height = 1;
// 
// Functions that draw basic primitives
//
// Outputs a string of text at the specified location.
void DrawText(double x, double y, char *string)
{
	void *font = GLUT_BITMAP_9_BY_15;

	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glEnable(GL_BLEND);

	int len, i;
	glRasterPos2d(x, y);
	len = (int) strlen(string);
	for (i = 0; i < len; i++) 
	{
		glutBitmapCharacter(font, string[i]);
	}

	glDisable(GL_BLEND);
}
void DrawLine(double x1, double y1, double x2, double y2)
{
	glBegin(GL_LINES);
	glVertex2d(x1, y1);
	glVertex2d(x2, y2);
	glEnd();
}
void DrawLine3d(double x1, double y1, double z1, double x2, double y2, double z2)
{
	glBegin(GL_LINES);
	glVertex3d(x1,y1,z1);
	glVertex3d(x2,y2,z2);
	glEnd();
}
void DrawSquare3d(double x1, double y1, double z1, double x2, double y2, double z2, double z3, double z4)
{
	glBegin(GL_QUADS);
	glVertex3d(x1,y1,z1);
	glVertex3d(x1,y2,z2);
	glVertex3d(x2,y2,z3);
	glVertex3d(x2,y1,z4);
	glEnd();
}
void DrawWater()
{
	
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glEnable(GL_BLEND);
	glColor4d(.1,.2,.9,.8);
	glBegin(GL_QUADS);
	glVertex3f(0,0,height);
	glVertex3f(0,N, height);
	glVertex3f(M,N,height);
	glVertex3f(M,0, height);
	glEnd();
	glDisable(GL_BLEND);
	/*
	glColor3d(0,0,1);
	glBegin(GL_QUADS);
	glVertex3d(0,0,height);
	glVertex3d(N,0,height);
	glVertex3d(N,M,height);
	glVertex3d(0,M,height);
	glEnd();
	*/
}
double getFramesPerSecond()
{
	static int frames = 0;
	static clock_t start_time = clock();
	//double seconds = start_time/CLOCKS_PER_SEC;

	frames++;
	clock_t current = clock();
	double ellapsed_time = (double)(current-start_time)/(double)CLOCKS_PER_SEC;
	double frames_per_second = (double) frames/ellapsed_time+.001;
	return frames_per_second;
}
//
// GLUT callback functions
//

// This callback function gets called by the Glut
// system whenever it decides things need to be redrawn.
double getHeight()
{
	double tempZ;
	double z = height;
	int cellX = (int)gRat.rx;
	int cellY = (int)gRat.ry;
	double offsetX = gRat.rx - cellX;
	double offsetY = gRat.ry - cellY;
	tempZ = gRat.gMaze.cells[cellX][cellY].HeightFunction(gRat.rx,gRat.ry);
	if(tempZ > height)
	{
		return tempZ;
	}
	else
	{
		return z;
	}
}
double getNextHeight()
{
	double tempZ;
	double z = height;
	int cellX = (int)gRat.rx;
	int cellY = (int)gRat.ry;
	double offsetX = gRat.rx - cellX;
	double offsetY = gRat.ry - cellY;
	tempZ = gRat.gMaze.cells[cellX][cellY].HeightFunction(gRat.getNextX(),gRat.getNextY());
	if(tempZ > height)
	{
		return tempZ;
	}
	else
	{
		return z;
	}
}
void display(void)
{
	double fps = getFramesPerSecond();
	gRat.deltaT = 1.0/fps;
	bool d2 = true;
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	DrawWater();
	if(!top)
	{
		//3d stuff next 3 lines
		glEnable(GL_DEPTH_TEST);//only for 3d
		glLoadIdentity();//only for 3d
		if(stad)
		{
			gluLookAt(M/2.0, -N/5.0, (M+N)/2,M/2.0,N/2.0, 1, 0, 0, 1);//for stadium view
		}
		else
		{
			gluLookAt(gRat.rx,gRat.ry,getHeight() + 1,gRat.getNextX(),gRat.getNextY(),getNextHeight() + .96,0,0,1);//rats view
		}
	}
	else
	{
		glDisable(GL_DEPTH_TEST);
		glLoadIdentity();
	}
	//glColor3d(0,0,1);// do something different with this color, glColor3ub(r,g,b) using the chaos random generator
	if(!top)
	{
		d2 = false;
	}
	gRat.gMaze.DrawMaze(d2);
	if(!first)
	{
		gRat.DrawRat();
	}
	if(gLeftButtonDown)
	{
		gRat.SpinLeft();//add degrees to spin left inside here
	}
	if(gRightButtonDown)
	{
		gRat.SpinRight();
	}

	if(gMiddleButtonDown)
	{
		if(gRat.legDegs >= 45)
		{
			legsDelta = -1.0;
		}
		else if(gRat.legDegs <= 10)
		{
			legsDelta = 1.0;
		}
		gRat.legDegs +=legsDelta;
		gRat.MoveForward();
	}
	glutSwapBuffers();
	glutPostRedisplay();
}

void reshape(int w, int h)
{
	// Reset our global variables to the new width and height.
	screen_x = w;
	screen_y = h;

	// Set the pixel resolution of the final picture (Screen coordinates).
	if(w > h)
		w = h;
	glViewport(0, 0, w, 1.0*N/M*w);

	// Set the projection mode to 2D orthographic, and set the world coordinates:
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	if(top)
	{
		gluOrtho2D(-.5, M+.5, -.5, N+.5);//size of the view
	}
	else
	{
		
		gluPerspective(30, double(w/h), .19, (M+N)*2);//this makes it 3d, change second to last back to 1 for stadium
		
	}
	glMatrixMode(GL_MODELVIEW);
}
// This callback function gets called by the Glut
// system whenever a key is pressed.
void keyboard(unsigned char c, int x, int y)
{
	switch (c) 
	{
	case 27: // escape character means to quit the program
		exit(0);
		break;
	case 'r'://rat
		// do something when 'b' character is hit.
		stad = false;
		top = false;
		first = true;
		reshape(screen_x, screen_y);
		break;
	case 't'://top
		stad = false;
		top = true;
		first = false;
		reshape(screen_x, screen_y);
		break;
	case 'y'://stadium
		stad = true;
		top = false;
		first = false;
		reshape(screen_x, screen_y);
		break;
	case 'q':
		height += .1;
		break;
	case 'w':
		height -= .1;
		break;
	default:
		return; // if we don't care, return without glutPostRedisplay()
	}

	glutPostRedisplay();
}


// This callback function gets called by the Glut
// system whenever the window is resized by the user.


// This callback function gets called by the Glut
// system whenever any mouse button goes up or down.
void mouse(int mouse_button, int state, int x, int y)
{
	if (mouse_button == GLUT_LEFT_BUTTON && state == GLUT_DOWN) 
	{
		gLeftButtonDown = true;
	}
	if (mouse_button == GLUT_LEFT_BUTTON && state == GLUT_UP) 
	{
		gLeftButtonDown = false;
		//gMiddleButtonDown = false;
	}
	if (mouse_button == GLUT_MIDDLE_BUTTON && state == GLUT_DOWN) 
	{
		gMiddleButtonDown = true;
	}
	if (mouse_button == GLUT_MIDDLE_BUTTON && state == GLUT_UP) 
	{
		gMiddleButtonDown = false;
	}
	if (mouse_button == GLUT_RIGHT_BUTTON && state == GLUT_DOWN) 
	{
		gRightButtonDown = true;
	}
	if (mouse_button == GLUT_RIGHT_BUTTON && state == GLUT_UP) 
	{
		gRightButtonDown = false;
		//gMiddleButtonDown = false;
	}
	glutPostRedisplay();
}
void keyPressed (unsigned char key, int x, int y) {  

}
// Your initialization code goes here.
void InitializeMyStuff()
{
	//gRat.gMaze.RemoveWalls(0,0);
	//gRat.gMaze.cells[0][0].mBottom = false;
	//gRat.gMaze.cells[M-1][N-1].mTop = false;
}


int main(int argc, char **argv)
{
	glutInit(&argc, argv);

	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
	glutInitWindowSize(screen_x, screen_y);
	glutInitWindowPosition(50, 50);

	int fullscreen = 0;
	if (fullscreen) 
	{
		glutGameModeString("800x600:32");
		glutEnterGameMode();
	} 
	else 
	{
		glutCreateWindow("3D Maze");
	}

	glutDisplayFunc(display);
	glutKeyboardFunc(keyboard);
	glutReshapeFunc(reshape);
	glutMouseFunc(mouse);


	glColor3d(0,0,0); // forground color
	glClearColor(.75, .75, .75, 0); // background color
	InitializeMyStuff();

	glutMainLoop();

	return 0;
}
