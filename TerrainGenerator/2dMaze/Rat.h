#pragma once
#include "Maze.h"
#include "Graphics.h"
	const double moveSize = .09;//multiply this by deltaT
	const double spinSize = .80;
	
class Rat 
{
public:
	Rat();
	void DrawRat();
	void SpinLeft();
	void SpinRight();
	void MoveForward();
	double getNextX();
	double getNextY();
	double getRadius();
	double rx, ry, rd, dx, dy;
	double legDegs, radius;
	Maze gMaze;
	double deltaT;


private:

};