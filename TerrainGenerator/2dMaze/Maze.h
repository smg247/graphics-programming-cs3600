#pragma once
#include "Cell.h"

const int M = 100;
const int N = 100;

class Maze 
{
private:
	
public:
	Maze();
	void DrawMaze(bool d2);
	Cell cells[M][N];
	bool isSafe(double nextX, double nextY, double radius);
};