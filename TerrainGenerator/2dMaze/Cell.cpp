#include <cmath>
#include "Cell.h"
#include "glut.h"
#include "Graphics.h"

Cell::Cell()
{
	mLeft = true;
	mRight = true;
	mTop = true;
	mBottom = true;
	mVisited = false;
	
}
double Cell::HeightFunction(double i, double j)//height is a function of i and j
	{
		double zScale = .2;
		double z = 2*sin(.54454*i) + 3*sin(.32445*j) + 3*sin(j*.43767) * 2*sin(.6548*i) + 2*sin(.2472*i) * 3*sin(.5382*j);//mess with this stuff to make randomly generated hills
		return z*zScale + 2;
	}
void Cell::DrawCell(int j, int i, bool d2)
{
	int r = 0;
	int g = 0;
	int b = 0;
	double z00 = HeightFunction(i,j);
	double z01 = HeightFunction(i,j+1);
	double z02 = HeightFunction(i+1,j+1);
	double z03 = HeightFunction(i+1,j);
	//if(mLeft && i == 0)
	//{
		r = (j*31785 + i*234576)%255;
		g = (j*31567 + i*234689)%255;
		b = (j*31345 + i*230976)%255;
		glColor3ub(r,g,b);
		
		
		DrawSquare3d(i,j,z00,i+1,j+1,z01, z02, z03);
		//DrawLine(i,j,i,j+1);

		glEnd();

	/*}
	if(mTop)
	{
		r = (j*31368 + i*234987)%255;
		g = (j*31123 + i*234489)%255;
		b = (j*31585 + i*230994)%255;
		glColor3ub(r,g,b);
		DrawLine3d(i,j+1,z00,i+1,j+1,z01);
		//DrawLine(i,j+1,i+1,j+1);
		
		glEnd();
	}
	if(mRight)
	{
		r = (j*31785 + i*234576)%255;
		g = (j*31567 + i*234689)%255;
		b = (j*31345 + i*230976)%255;
		glColor3ub(r,g,b);
		DrawLine3d(i+1,j+1,z00,i+1,j,z01);
		//DrawLine(i+1,j+1,i+1,j);
		
		glEnd();
	}
	if(mBottom && j == 0)
	{
		r = (j*31368 + i*234987)%255;
		g = (j*31123 + i*234489)%255;
		b = (j*31585 + i*230994)%255;
		glColor3ub(r,g,b);
		DrawLine3d(i,j,z00,i+1,j,z01);
		//DrawLine(i,j,i+1,j);
		
		glEnd();
	}*/
	

}
