#include <iostream>
#include <ctime>
#include "Maze.h"
#include "Cell.h"
int temp;

Maze::Maze()
{
	srand(time(0));

}
void Maze::DrawMaze(bool d2)
{

	for(int i = 0; i < M; i++)
	{
		for(int j = 0; j < N; j++)
		{
			if(d2)
			{
				cells[i][j].DrawCell(j,i,true);
			}
			else
			{
				cells[i][j].DrawCell(j,i,false);
			}
		}
	}
}

bool Maze::isSafe(double nextX, double nextY, double radius)
{
	int cellX = (int)nextX;
	int cellY = (int)nextY;
	double offsetX = nextX - cellX;
	double offsetY = nextY - cellY;
	if(cells[cellX][cellY].mRight && offsetX +radius >= 1.0)//testing the right wall
	{
		return false;
	}
	else if(cells[cellX][cellY].mLeft && offsetX -radius <= 0)//testing the left wall
	{
		return false;
	}
	else if(cells[cellX][cellY].mTop && offsetY +radius >= 1.0)//testing the top wall
	{
		return false;
	}
	else if(cells[cellX][cellY].mBottom && offsetY -radius <= 0)//testing the bottom wall
	{
		return false;
	}
	//need cases for corners too

	else
	{
		return true;
	}

}