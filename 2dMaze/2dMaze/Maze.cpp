#include <iostream>
#include <ctime>
#include "Maze.h"
#include "Cell.h"
int temp;

Maze::Maze()
{
	srand(time(0));

}
void Maze::DrawMaze(bool d2)
{

	for(int i = 0; i < M; i++)
	{
		for(int j = 0; j < N; j++)
		{
			if(d2)
			{
				cells[i][j].DrawCell(j,i,true);
			}
			else
			{
				cells[i][j].DrawCell(j,i,false);
			}
		}
	}
}
void Maze::RemoveWalls(int i, int j)
{
	cells[i][j].mVisited = true;
	while(true)
	{
	// first see how many choices we have
		int count = 0;
		int nextI[4];
		int nextJ[4];
		// Left?
		if(i -1 >= 0 && cells[i-1][j].mVisited == false)
		{
			nextI[count] = i -1;
			nextJ[count] = j;
			count++;
		}
		// UP?
		if(j +1 < N && cells[i][j+1].mVisited == false)
		{
			nextI[count] = i;
			nextJ[count] = j+1;
			count++;
		}
		// Right?
		if(i + 1 < M && cells[i+1][j].mVisited == false)
		{
			nextI[count] = i +1;
			nextJ[count] = j;
			count++;
		}
		// down?
		if(j -1 >= 0 && cells[i][j-1].mVisited == false)
		{
			nextI[count] = i;
			nextJ[count] = j-1;
			count++;
		}
	// if none, then return.
		if(count==0)
		{
			return;
		}
	// randomly pick one of them.
		int dirChoice = (rand()%count);
		if(nextI[dirChoice] == i-1 && nextJ[dirChoice] == j)//left
		{
			cells[i][j].mLeft = false;
			cells[i-1][j].mRight = false;
			RemoveWalls(i-1,j);
		}
		else if(nextI[dirChoice] == i && nextJ[dirChoice] == j+1)//top
		{
			cells[i][j].mTop = false;
			cells[i][j+1].mBottom = false;
			RemoveWalls(i,j+1);
		}
		else if(nextI[dirChoice] == i+1 && nextJ[dirChoice] == j)//right
		{
			cells[i][j].mRight = false;
			cells[i+1][j].mLeft = false;
			RemoveWalls(i+1,j);
		}
		else if(nextI[dirChoice] == i && nextJ[dirChoice] == j-1)//bottom
		{
			cells[i][j].mBottom = false;
			cells[i][j-1].mTop = false;
			RemoveWalls(i,j-1);
		}

	// Remove walls between current cell and the randomly picked cell.
	
	// Recurse to that cell
	}
}
bool Maze::isSafe(double nextX, double nextY, double radius)
{
	int cellX = (int)nextX;
	int cellY = (int)nextY;
	double offsetX = nextX - cellX;
	double offsetY = nextY - cellY;
	if(cells[cellX][cellY].mRight && offsetX +radius >= 1.0)//testing the right wall
	{
		//cells[cellX][cellY].mRight = false;
		//cells[cellX+1][cellY].mLeft = false;
		return false;
	}
	else if(cells[cellX][cellY].mLeft && offsetX -radius <= 0)//testing the left wall
	{
		//cells[cellX][cellY].mLeft = false;
		//cells[cellX-1][cellY].mRight = false;
		return false;
	}
	else if(cells[cellX][cellY].mTop && offsetY +radius >= 1.0)//testing the top wall
	{
		//cells[cellX][cellY].mTop = false;
		//cells[cellX][cellY+1].mBottom = false;
		return false;
	}
	else if(cells[cellX][cellY].mBottom && offsetY -radius <= 0)//testing the bottom wall
	{
		//cells[cellX][cellY].mBottom = false;
		//cells[cellX][cellY-1].mTop = false;
		return false;
	}
	//need cases for corners too

	else
	{
		return true;
	}

}