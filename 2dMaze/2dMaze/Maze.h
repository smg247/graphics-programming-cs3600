#pragma once
#include "Cell.h"

const int M = 15;
const int N = 15;

class Maze 
{
private:
	
public:
	Maze();
	void RemoveWalls(int i, int j);
	void DrawMaze(bool d2);
	Cell cells[M][N];
	bool isSafe(double nextX, double nextY, double radius);
};