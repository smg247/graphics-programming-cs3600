// OpenGL/GLUT starter kit for Windows 7 and Visual Studio 2010
// Created spring, 2011
//
// This is a starting point for OpenGl applications.
// Add code to the "display" function below, or otherwise
// modify this file to get your desired results.
//
// For the first assignment, add this file to an empty Windows Console project
//		and then compile and run it as is.
// NOTE: You should also have glut.h,
// glut32.dll, and glut32.lib in the directory of your project.
// OR, see GlutDirectories.txt for a better place to put them.

//This program creates a 2d maze
//Stephen Goeddel 
//February 2012
#include <cstdlib>//make exit not complain
#include <cmath>
#include <cstring>
#include <ctime>
#include <iostream>
#include <ctype.h>
#include <conio.h>
using namespace std;
#include "glut.h"
#include "Cell.h"
#include "Maze.h"
#include "Graphics.h"
#include "Rat.h"
#include "Tga.h"

// Global Variables (Only what you need!)
double screen_x = 700;
double screen_y = 700;
//Maze gMaze = Maze();
Rat gRat = Rat();
bool gLeftButtonDown = false;
bool gMiddleButtonDown = false;
bool gRightButtonDown = false;
bool newMaze = false;
double legsDelta = 1.0;
double eyeLvl = .1;
double look = .101;
bool top = true;
bool stad = false;
bool first = false;

const int num_textures = 7;
static GLuint texName[num_textures];
int GetTextureNumber(int i) //may need this...
{
	return texName[i];
}

 //tga image loader code.

gliGenericImage *readTgaImage(char *filename)
{
  FILE *file;
  gliGenericImage *image;

  file = fopen(filename, "rb");
  if (file == NULL) {
    printf("Error: could not open \"%s\"\n", filename);
    return NULL;
  }
  image = gliReadTGA(file, filename);
  fclose(file);
  if (image == NULL) {
    printf("Error: could not decode file format of \"%s\"\n", filename);
    return NULL;
  }
  return image;
}

void Interpolate(double t1, double t, double t2, double x1, double &x, double x2)//moves the pieces
{
	if(t<t1)
	{
		x = x1;
		return;
	}
	if(t >t2)
	{
		x = x2;
		return;
	}
	double ratio = (t - t1)/(t2-t1);
	x = x1 + (x2-x1)*ratio;
}
// Generic image loader code.
gliGenericImage *readImage(char *filename)
{
	size_t size = strlen(filename);
	if(toupper(filename[size-3]) == 'T' && toupper(filename[size-2]) == 'G' && toupper(filename[size-1]) == 'A')
	{
		gliGenericImage * result = readTgaImage(filename);
		if(!result)
		{
			cerr << "Error opening " << filename << endl;
			_getch();
			exit(1);
		}
		return result;
	}
	else
	{
		cerr << "Unknown Filetype!\n";
		_getch();
		exit(1);
	}
}

// This resets the edges of the texture image to a given "border color".
// You must call this for clamped images that do not take up the whole polygon.
// Otherwise, the texture edges will smear outward across the rest
// of the polygon.
void SetBorder(gliGenericImage * image)
{
	// set a border color.
	unsigned int border_r=200;
	unsigned int border_g=200;
	unsigned int border_b=200;
	int x,y;
	y=0;
	for(x=0; x<image->width; x++)
	{
		image->pixels[ 3*(y*image->width + x) + 0]=border_r;
		image->pixels[ 3*(y*image->width + x) + 1]=border_g;
		image->pixels[ 3*(y*image->width + x) + 2]=border_b;
	}
	y=1;
	for(x=0; x<image->width; x++)
	{
		image->pixels[ 3*(y*image->width + x) + 0]=border_r;
		image->pixels[ 3*(y*image->width + x) + 1]=border_g;
		image->pixels[ 3*(y*image->width + x) + 2]=border_b;
	}
	y=image->height-1;
	for(x=0; x<image->width; x++)
	{
		image->pixels[ 3*(y*image->width + x) + 0]=border_r;
		image->pixels[ 3*(y*image->width + x) + 1]=border_g;
		image->pixels[ 3*(y*image->width + x) + 2]=border_b;
	}
	y=image->height-2;
	for(x=0; x<image->width; x++)
	{
		image->pixels[ 3*(y*image->width + x) + 0]=border_r;
		image->pixels[ 3*(y*image->width + x) + 1]=border_g;
		image->pixels[ 3*(y*image->width + x) + 2]=border_b;
	}

	x=0;
	for(y=0; y<image->height; y++)
	{
		image->pixels[ 3*(y*image->width + x) + 0]=border_r;
		image->pixels[ 3*(y*image->width + x) + 1]=border_g;
		image->pixels[ 3*(y*image->width + x) + 2]=border_b;
	}
	x=1;
	for(y=0; y<image->height; y++)
	{
		image->pixels[ 3*(y*image->width + x) + 0]=border_r;
		image->pixels[ 3*(y*image->width + x) + 1]=border_g;
		image->pixels[ 3*(y*image->width + x) + 2]=border_b;
	}
	x=image->width-1;
	for(y=0; y<image->height; y++)
	{
		int index = 3*(y*image->width + x);
		image->pixels[ index + 0]=border_r;
		image->pixels[ index + 1]=border_g;
		image->pixels[ index + 2]=border_b;
	}
	x=image->width-2;
	for(y=0; y<image->height; y++)
	{
		int index = 3*(y*image->width + x);
		image->pixels[ index + 0]=border_r;
		image->pixels[ index + 1]=border_g;
		image->pixels[ index + 2]=border_b;
	}
}
void DrawText(double x, double y, char *string)
{
	void *font = GLUT_BITMAP_9_BY_15;

	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glEnable(GL_BLEND);

	int len, i;
	glRasterPos2d(x, y);
	len = (int) strlen(string);
	for (i = 0; i < len; i++) 
	{
		glutBitmapCharacter(font, string[i]);
	}

	glDisable(GL_BLEND);
}
void DrawLine(double x1, double y1, double x2, double y2)
{
	glBegin(GL_LINES);
	glVertex2d(x1, y1);
	glVertex2d(x2, y2);
	glEnd();
}

//
// GLUT callback functions
//

// This callback function gets called by the Glut
// system whenever it decides things need to be redrawn.
void display(void)
{
	
	bool d2 = true;
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	if(!top)
	{
		//3d stuff next 3 lines
		glEnable(GL_DEPTH_TEST);//only for 3d
		glLoadIdentity();//only for 3d
		if(stad)
		{
			gluLookAt(M/2.0, -N/2.0, M+N,gRat.getNextX(),gRat.getNextY(), .01, 0, 0, 1);//for stadium view
		}
		else
		{
			gluLookAt(gRat.rx,gRat.ry,eyeLvl,gRat.getNextX(),gRat.getNextY(),look,0,0,1);//rats view
		}
	}
	else
	{
		glDisable(GL_DEPTH_TEST);
		glLoadIdentity();
	}
	//glColor3d(0,0,1);// do something different with this color, glColor3ub(r,g,b) using the chaos random generator
	if(!top)
	{
		d2 = false;
	}
	gRat.gMaze.DrawMaze(d2);
	if(!first)
	{
		gRat.DrawRat();
	}
	if(gLeftButtonDown)
	{
		gRat.SpinLeft();//add degrees to spin left inside here
	}
	if(gRightButtonDown)
	{
		gRat.SpinRight();
	}

	if(gMiddleButtonDown)
	{
		if(gRat.legDegs >= 45)
		{
			legsDelta = -1.0;
		}
		else if(gRat.legDegs <= 10)
		{
			legsDelta = 1.0;
		}
		gRat.legDegs +=legsDelta;
		gRat.MoveForward();
	}
	/*if(!d2)//dont do any of this in 2d mode
	{
		glEnable(GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D, texName[1]);//ground.tga

		glBegin(GL_QUADS);//the bottom of the maze
		glTexCoord2f(0,0); glVertex3d(0 ,N + 5, 0);
		glTexCoord2f(1,0); glVertex3d(M + 5,N + 5, 0);
		glTexCoord2f(1,1); glVertex3d(M + 5,N+5 , 5);
		glTexCoord2f(0,1); glVertex3d(0,N + 5, 5);
		glEnd();

		glDisable(GL_TEXTURE_2D);
	}*/
	glutSwapBuffers();
	glutPostRedisplay();
}

void reshape(int w, int h)
{
	// Reset our global variables to the new width and height.
	screen_x = w;
	screen_y = h;

	// Set the pixel resolution of the final picture (Screen coordinates).
	if(w > h)
		w = h;
	glViewport(0, 0, w, 1.0*N/M*w);

	// Set the projection mode to 2D orthographic, and set the world coordinates:
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	if(top)
	{
		gluOrtho2D(-.5, M+.5, -.5, N+.5);//size of the view
	}
	else
	{
		
		gluPerspective(30, double(w/h), .19, (M+N)*2);//this makes it 3d, change second to last back to 1 for stadium
		
	}
	glMatrixMode(GL_MODELVIEW);
}
// This callback function gets called by the Glut
// system whenever a key is pressed.
void keyboard(unsigned char c, int x, int y)
{
	switch (c) 
	{
	case 27: // escape character means to quit the program
		exit(0);
		break;
	case 'r'://rat
		stad = false;
		top = false;
		first = true;
		reshape(screen_x, screen_y);
		break;
	case 't'://top
		stad = false;
		top = true;
		first = false;
		reshape(screen_x, screen_y);
		break;
	case 'y'://stadium
		stad = true;
		top = false;
		first = false;
		reshape(screen_x, screen_y);
		break;
	case 'u'://up
		look += .001;
		if(look > .99)
			look = .99;
		break;
	case 'd'://down
		look -= .001;
		if(look < 0)
			look = 0;
		break;
	case 'a'://upward rat
		eyeLvl += .01;
		look = eyeLvl;
		if(eyeLvl > .99)
			eyeLvl = .99;
		break;
		case 's'://downward rat
		eyeLvl -= .01;
		look = eyeLvl;
		if(eyeLvl < .1)
			eyeLvl = .1;
		break;
	default:
		return; // if we don't care, return without glutPostRedisplay()
	}

	glutPostRedisplay();
}


// This callback function gets called by the Glut
// system whenever the window is resized by the user.


// This callback function gets called by the Glut
// system whenever any mouse button goes up or down.
void mouse(int mouse_button, int state, int x, int y)
{
	if (mouse_button == GLUT_LEFT_BUTTON && state == GLUT_DOWN) 
	{
		gLeftButtonDown = true;
	}
	if (mouse_button == GLUT_LEFT_BUTTON && state == GLUT_UP) 
	{
		gLeftButtonDown = false;
		//gMiddleButtonDown = false;
	}
	if (mouse_button == GLUT_MIDDLE_BUTTON && state == GLUT_DOWN) 
	{
		gMiddleButtonDown = true;
	}
	if (mouse_button == GLUT_MIDDLE_BUTTON && state == GLUT_UP) 
	{
		gMiddleButtonDown = false;
	}
	if (mouse_button == GLUT_RIGHT_BUTTON && state == GLUT_DOWN) 
	{
		gRightButtonDown = true;
	}
	if (mouse_button == GLUT_RIGHT_BUTTON && state == GLUT_UP) 
	{
		gRightButtonDown = false;
		//gMiddleButtonDown = false;
	}
	glutPostRedisplay();
}
bool PowerOf2(int h)
{
	if(h!= 2 && h!=4 && h!=8 && h!=16 && h!=32 && h!=64 && h!=128 && 
				h!=256 && h!=512 && h!=1024 && h!=2048 && h!=4096)
		return false;
	else
		return true;
}
// Your initialization code goes here.
void InitializeMyStuff()
{
	gRat.gMaze.RemoveWalls(0,0);
	gRat.gMaze.cells[0][0].mBottom = false;
	gRat.gMaze.cells[M-1][N-1].mTop = false;

	gliGenericImage *image[num_textures];
	int n=0;
	image[n++] = readImage("fruit.tga");
	image[n++] = readImage("sky.tga");
	image[n++] = readImage("cookies.tga");
	image[n++] = readImage("Seattle.tga");
	image[n++] = readImage("ground.tga");
	image[n++] = readImage("master.tga");
	image[n++] = readImage("stone.tga");
	if(n!=num_textures)
	{
		printf("Error: Wrong number of textures\n");
		_getch();
		exit(1);;
	}

	glGenTextures(num_textures, texName);

	for(int i=0; i<num_textures; i++)
	{
		glBindTexture(GL_TEXTURE_2D, texName[i]);
		glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_DECAL);
		int repeats = false;
		if(i == 6)//the stone texture
		{
			repeats = true;
		}
		int needs_border = false; // Needed if clamping and not filling the whole polygon.
		if(i == 4)
		{
			needs_border = true;
		}
		if(repeats)
		{
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		}
		else
		{
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
		}
		if(needs_border)
		{
			// set a border.
			SetBorder(image[i]);
		}

		bool mipmaps = false;
		if(!PowerOf2(image[i]->height) || !PowerOf2(image[i]->width))
		{
			// WARNING: Images that do not have width and height as 
			// powers of 2 MUST use mipmaps.
			mipmaps = true; 
		}

		if (mipmaps) 
		{
			gluBuild2DMipmaps(GL_TEXTURE_2D, image[i]->components,
					image[i]->width, image[i]->height,
					image[i]->format, GL_UNSIGNED_BYTE, image[i]->pixels);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,
					//GL_LINEAR_MIPMAP_LINEAR);
					GL_NEAREST);//choosing between speed and quality
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, 
					//GL_LINEAR);
					GL_NEAREST);//choosing between speed and quality
		} 
		else 
		{
			glTexImage2D(GL_TEXTURE_2D, 0, image[i]->components,
					image[i]->width, image[i]->height, 0,
					image[i]->format, GL_UNSIGNED_BYTE, image[i]->pixels);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		}
	}

}


int main(int argc, char **argv)
{
	glutInit(&argc, argv);

	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
	glutInitWindowSize(screen_x, screen_y);
	glutInitWindowPosition(50, 50);

	int fullscreen = 0;
	if (fullscreen) 
	{
		glutGameModeString("800x600:32");
		glutEnterGameMode();
	} 
	else 
	{
		glutCreateWindow("3D Maze");
	}

	glutDisplayFunc(display);
	glutKeyboardFunc(keyboard);
	glutReshapeFunc(reshape);
	glutMouseFunc(mouse);


	glColor3d(0,0,0); // forground color
	glClearColor(.25, .25, .25, 0); // background color
	InitializeMyStuff();
	cout << "The following keys are useful for playing this game: "<< endl << "R: for rat view" << endl << "T: for top view" << endl
		<< "Y: for stadium view" << endl << "U: for looking up" << endl << "D: for looking down" << endl << "A: for going up" << endl << "s: for going down" << endl;
	system("PAUSE");
	glutMainLoop();

	return 0;
}
