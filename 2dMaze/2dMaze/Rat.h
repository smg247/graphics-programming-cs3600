#pragma once
#include "Maze.h"
	const double moveSize = .04;
	const double spinSize = .80;
	
class Rat 
{
public:
	Rat();
	void DrawRat();
	void SpinLeft();
	void SpinRight();
	void MoveForward();
	double getNextX();
	double getNextY();
	double getRadius();
	double rx, ry, rd, dx, dy;
	double legDegs, radius;
	Maze gMaze;


private:

};