#include <ctime>
#include "Cell.h"
#include "glut.h"
#include "Graphics.h"

Cell::Cell()
{
	mLeft = true;
	mRight = true;
	mTop = true;
	mBottom = true;
	mVisited = false;
	
}
void Cell::DrawCell(int j, int i, bool d2)
{
	static clock_t t1 = clock();
	clock_t t2 = clock();
	double t = ((double)t2-t1)/CLOCKS_PER_SEC;
	int r = .25;
	int g = .25;
	int b = .25;

	if(!d2)
	{
	//lets draw the ground
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, GetTextureNumber(4));//ground.tga

	glBegin(GL_QUADS);//the bottom of the maze
	glTexCoord2f(.15,.15); glVertex2d(i + .15,j + .15);
	glTexCoord2f(.85,.15); glVertex2d(i + .15,j + .85);
	glTexCoord2f(.85,.85); glVertex2d(i + .85,j + .85);
	glTexCoord2f(0.15,.85); glVertex2d(i + .85,j + .15);
	glEnd();

	glDisable(GL_TEXTURE_2D);
	}
	if(mLeft)
	{
		/*r = (j*31785 + i*234576)%255;
		g = (j*31567 + i*234689)%255;
		b = (j*31345 + i*230976)%255;
		*/
		glColor3ub(r,g,b);
		if(d2)
		{
		DrawLine(i,j,i,j+1);
		}
		else
		{
		glBegin(GL_QUADS);
		glVertex3d(i,j,0);
		glVertex3d(i,j+1,0);
		glVertex3d(i,j+1,1);
		glVertex3d(i,j,1);
		glEnd();
		glEnable(GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D, GetTextureNumber(6));//stone.tga

		glBegin(GL_QUADS);//the bottom of the maze
		glTexCoord2f(0,0); glVertex3d(i+.01,j+.999,0);
		glTexCoord2f(2,0); glVertex3d(i+.01,j+.999,1);
		glTexCoord2f(2,2); glVertex3d(i+.01,j+.01,1);
		glTexCoord2f(0,2); glVertex3d(i+.01,j+.01,0);
		glEnd();

		glDisable(GL_TEXTURE_2D);
		}
		glEnd();

	}
	if(mTop)
	{
		/*r = (j*31368 + i*234987)%255;
		g = (j*31123 + i*234489)%255;
		b = (j*31585 + i*230994)%255;
		*/
		glColor3ub(r,g,b);
		if(d2)
		{
		DrawLine(i,j+1,i+1,j+1);
		}
		else
		{
		glBegin(GL_QUADS);
		glVertex3d(i,j+1,0);
		glVertex3d(i+1,j+1,0);
		glVertex3d(i+1,j+1,1);
		glVertex3d(i,j+1,1);
		glEnd();
		double height = 0;
		glEnable(GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D, GetTextureNumber(5));//master.tga
		Interpolate(0,t,12,0,height,1);
		glBegin(GL_QUADS);//the bottom of the maze
		glTexCoord2f(0,0); glVertex3d(i,j+.999,height);
		glTexCoord2f(1,0); glVertex3d(i,j+.999,1);
		glTexCoord2f(1,1); glVertex3d(i+.999,j+.999,1);
		glTexCoord2f(0,1); glVertex3d(i+.999,j+.999,height);
		glEnd();

		glDisable(GL_TEXTURE_2D);
		}
		glEnd();
	}
	if(mRight)
	{
		/*r = (j*31785 + i*234576)%255;
		g = (j*31567 + i*234689)%255;
		b = (j*31345 + i*230976)%255;
		*/
		glColor3ub(r,g,b);
		if(d2)
		{
		DrawLine(i+1,j+1,i+1,j);
		}
		else
		{
		glBegin(GL_QUADS);
		glVertex3d(i+1,j+1,0);
		glVertex3d(i+1,j,0);
		glVertex3d(i+1,j,1);
		glVertex3d(i+1,j+1,1);
		glEnd();
		glEnable(GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D, GetTextureNumber(6));//stone.tga

		glBegin(GL_QUADS);//the bottom of the maze
		glTexCoord2f(0,0); glVertex3d(i+.999,j+.999,0);
		glTexCoord2f(2,0); glVertex3d(i+.999,j+.999,1);
		glTexCoord2f(2,2); glVertex3d(i+.999,j,1);
		glTexCoord2f(0,2); glVertex3d(i+.999,j,0);
		glEnd();

		glDisable(GL_TEXTURE_2D);
		}
		glEnd();
	}
	if(mBottom)
	{
		/*r = (j*31368 + i*234987)%255;
		g = (j*31123 + i*234489)%255;
		b = (j*31585 + i*230994)%255;
		*/
		glColor3ub(r,g,b);
		if(d2)
		{
		DrawLine(i,j,i+1,j);
		}
		else
		{
		glBegin(GL_QUADS);
		glVertex3d(i,j,0);
		glVertex3d(i+1,j,0);
		glVertex3d(i+1,j,1);
		glVertex3d(i,j,1);
		glEnd();

		glEnable(GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D, GetTextureNumber(5));//master.tga

		glBegin(GL_QUADS);//the bottom of the maze
		glTexCoord2f(0,0); glVertex3d(i,j + .01,0);
		glTexCoord2f(1,0); glVertex3d(i,j + .01,1);
		glTexCoord2f(1,1); glVertex3d(i+.999,j + .01,1);
		glTexCoord2f(0,1); glVertex3d(i+.999,j + .01,0);
		glEnd();

		glDisable(GL_TEXTURE_2D);
		}
		glEnd();
	}
	

}
