Documentation for Springs

type in "Particle" followed by its x, y, xDir, yDir, radius, and 0 for not anchored 1 for anchored (no commas, spaces only) to set a particle

type in "Spring" followed by its first connected particle, second connected particle, spring constant, damping constant, red number, green number
, blue number, and rest length for a spring

type in "Gravity" followed by its x force, and y force, for gravity

type in "Drag" followed by its friction for resistance

type in "DeltaT" followed by the value you would like it set to, to change DeltaT

Comments are supported!